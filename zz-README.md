# Vorrenrey

**Status:** Work In Progress, not considered safe in a production environment

Standalone freedom-oriented fork of github:VoronDesign/Voron-2 version 2.4 dedicated to:
1. Addressing the GPLv2 license violations by removing the hard dependency on non-free libraries that do not fit the definition of system libraries definitions [https://github.com/VoronDesign/Voron-2/issues/308] and creditting all known contributors to the Free Hardware Design.
2. Making the design more functional and robust through implementation of Quality Assurance ("QA") routines enabled by using Functional Computer-Aided Engineering ("fCAE") and simulations in FreeCAD to be sustainable in a production environment.
3. Optimization of the design for fabrication and consumer distribution
4. Modularization of the design

### Adopted resolution of the license violations

This project adopts zero-tolerance to the license violation of General Public License by Richard Matthew Stallman of The Free Software Foundation, Inc.

The original VoronDesign's design is released under GPLv3 which violates the RepRap Machine v1.0 ("Darwin") License (GPLv2) from which this design is forked from as not all developers have agreed to the license change as such the GPLv3 license used by VoronDesign is considered voided for the violation of the Copyrighth Law in the United States, European Union and states honoring the Copyright Law.

Due to the violation of GPLv2 all contributions to the VoronDesign's Voron-2 are reclaimed under GPLv2 by The RepRap project If you contributed to the project then feel free to reclaim your contribution under GPLv2 while consider consenting to the use of GPLv3-or-later.

We eventually want to upgrade the license on GPLv3-or-later once we get the approval of all known contributors to the relevant Free Hardware Designs.

The replacement of the hard dependencies on non-free libraries that do not fit the defitniion of system libraries under GPLv2 are currently being replaced with highest priority and you are invitted to join the effort by addressing the issue tracking according to the milestones.

### Credits

The VoronDesign didn't bother to maintain the license history so we did our best to recover it. This recreation may not be perfect and if you can prove that you:
1. Worked with the RepRap project on the RepRap machine up to the version 1.0 ("Darwin")
2. Cooperated with Liam E. Moyer on the CoreXY standard

#### Standard

Using the open-standard of CoreXY by [Lian E. Moyer](https://web.mit.edu/imoyer/www/index.html) of Massachusetts Institute of Technology on <https://corexy.com>

#### Free Hardware Design
1. (2004) This Free Hardware Design would not be possible without the work of [Dr. Adrian Bowyer](https://en.wikipedia.org/wiki/Adrian_Bowyer) and contributors to [The RepRap Project](https://en.wikipedia.org/wiki/RepRap) which enabled the Free Hardware Design additive manufacturing machines.
2. (2012) This design is a fork of RepRap Machine v1.0 (Darwin) - https://corexy.com
3. (2016) VoronDesign's original release of Voron-2 - https://github.com/VoronDesign/Voron-2/commit/d3de99b277992dc90e4c2bbdc0bc1fcbd1b8f873 
