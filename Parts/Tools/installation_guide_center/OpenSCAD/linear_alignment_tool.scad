/*
 * linear_alignment_tool.scad - library for generating the linear alignment tool for vorrenrey CNC machine
 *
 * Licensed under the terms and conditions of GPLv2 license <https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 * - Copyright 2006 The RepRap Project <https://reprap.org> and contributors
 * - Copyright 2012 Liam E. Moyer <imoyer@mit.edu>
 * - Copyright 2016 VoronDesigns <https://vorondesign.com/> and contributors 
 * - Copyright 2022 Jacob Hrbek <kreyren@rixotstudio.cz>
 *
 * Licensed under the terms and conditions of GPLv3 license <https://www.gnu.org/licenses/gpl-3.0.en.html>
 * - Copyright 2016 VoronDesigns <https://vorondesign.com/> and contributors
 *
 * Licensed under the terms and conditions of GPLv3-or-later license <https://www.gnu.org/licenses/gpl-3.0.en.html>
 * - Copyright 2022 Jacob Hrbek <kreyren@rixotstudio.cz>
 */
 
lat_height = 50;


// FIXME(Krey): The object is not fully functional as it uses static values
// FIXME-QA(Krey): The original design has campers that are not implemented here
module linear_alignment_tool() {
     linear_extrude(lat_height) {
        polygon(
            points = [
                /*
                           X    .
                */
                [-4.6,0],
                /*
                           O    .
                           |
                           X            
                */
               [-4.6,-6.5],
               /*
                           O    .
                           |
                           O---------X
               */
               [4.6,-6.5],
               /*
                           O    .    X
                           |         |
                           O---------O
               */
               [4.6,0],
               /*
                           O    .    O---X
                           |         |
                           O---------O
               */
               [10.10,0],
               /*
                                         X
                                         |
                                         |
                           O    .    O---O
                           |         |
                           O---------O
               */
               [10.10,10.21],
               /*
                                        O--X
                                        |
                                        |
                          O    .    O---O
                          |         |
                          O---------O
               */
               [13,10.21],
               /*
                                        O--O
                                        |  |
                                        |  |
                          O    .    O---O  |
                          |         |      |
                          O---------O      |
                                           |
                                           |
                                           X
               */
               [13,-10.21],
               /*
                                        O--O
                                        |  |
                                        |  |
                          O    .    O---O  |
                          |         |      |
                          O---------O      |
                                           |
                                           |
                    X----------------------O
               */
               [-13,-10.21],
               /*
                    X                   O--O
                    |                   |  |
                    |                   |  |
                    |     O    .    O---O  |
                    |     |         |      |
                    |     O---------O      |
                    |                      |
                    |                      |
                    O----------------------O
               */
               [-13,10.21],
               /*
                    O--X                O--O
                    |                   |  |
                    |                   |  |
                    |     O    .    O---O  |
                    |     |         |      |
                    |     O---------O      |
                    |                      |
                    |                      |
                    O----------------------O
               */
               [-10.10,10.21],
               /*
                   O--O                 O--O
                   |  |                 |  |
                   |  |                 |  |
                   |  X   O    .    O---O  |
                   |      |         |      |
                   |      O---------O      |
                   |                       |
                   |                       |
                   O-----------------------O
               */
               [-10.10,0],
            ]
        );
     };
 }
 
 linear_alignment_tool();
